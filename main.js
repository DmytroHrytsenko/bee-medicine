require.config({
	paths: {
		//libraries

		'jquery' : 'libraries/jquery',
		'underscore' : 'libraries/underscore',
		'backbone' : 'libraries/backbone',
		'backbone-mediator' : 'libraries/backbone-mediator',
		
		//service module

		'ServModel' : 'js/Services/ServiceModel',
		'ServViewCust' : 'js/Services/ServiceViewCustomer',
		'ServViewAdm' : 'js/Services/ServiceViewAdmin',
		'ServCollection' : 'js/Services/ServiceCollection',
		'ServCollectionViewCust' : 'js/Services/ServiceCollectionViewCustomer',
		'ServCollectionViewAdm' : 'js/Services/ServiceCollectionViewAdmin',

		'servTemplCustomer' : 'js/Services/templates/serviceTemplateCustomer',
		'servTemplAdmin' : 'js/Services/templates/serviceTemplateAdmin',
		'servEditTempl' : 'js/Services/templates/serviceEditTemplate',
		'servAddTempl' : 'js/Services/templates/serviceAddTemplate',

		//conacts module

		'ContactsModel' : 'js/Contacts/ContactsModel',
		'ContactsViewCustomer' : 'js/Contacts/ContactsViewCustomer',
		'ContactsViewAdmin' : 'js/Contacts/ContactsViewAdmin',
		'ContactsCollection' : 'js/Contacts/ContactsCollection',
		'ContactsCollectionViewCustomer' : 'js/Contacts/ContactsCollectionViewCustomer',
		'ContactsCollectionViewAdmin' : 'js/Contacts/ContactsCollectionViewAdmin',

		'contactsCustomerTemplate' : 'js/Contacts/templates/contactsCustomerTemplate',
		'contactsAdminTemplate' : 'js/Contacts/templates/contactsAdminTemplate',
		'contactsAdminEditTemplate' : 'js/Contacts/templates/contactsAdminEditTemplate',

		//book module

		'BookTimeModel' : 'js/Contacts/BookTimeModel',
		'BookTimeView' : 'js/Contacts/BookTimeView',
		'bookTimeTemplate' : 'js/Contacts/templates/bookTimeTemplate',

		//recievers module

		'RecieverModel' : 'js/Recievers/RecieverModel',
		'RecieversCollection' : 'js/Recievers/RecieversCollection',
		'RecieversViewAdmin' : 'js/Recievers/RecieverViewAdmin',
		'RecieversCollectionViewAdmin' : 'js/Recievers/RecieversCollectionViewAdmin',

		'recieversAddTemplate' : 'js/Recievers/templates/recieverAddTemplate',
		'recieversEditTemplate' : 'js/Recievers/templates/recieverEditTemplate',
		'recieversTemplateAdmin' : 'js/Recievers/templates/recieverTemplateAdmin',

		//router

		'Router' : 'js/Router',
		
		// Loader

		'LoaderView' : 'js/LoaderView'
	}
});

require(['jquery', 'backbone', 'LoaderView', 'Router'], function($, Backbone, LoaderView, Router) {
	var router = new Router();
	Backbone.history.start({ pushState: true });
	if (document.readyState !== "complete") {
		$(window).load(function() {
			loaderView.remove();
		});
		var loaderView = new LoaderView();
	}
	
	

});

