define(['jquery', 'underscore', 'backbone', 
	'RecieversCollection', 'recieversAddTemplate', 
	'RecieverModel', 'RecieversViewAdmin'], function($, _, Backbone, ReceiversCollection, receiverAddTemplate, Reciever, RecieverViewAdmin) {	
	function resetInput() {
		this.$(this.defaults.recieverClass).val('');
	};
	var RecieversCollectionViewAdmin = Backbone.View.extend({
		tagName: 'div',
		className: 'admin-block',
		addTemplate: receiverAddTemplate,

		initialize: function() {
			this.collection = new ReceiversCollection();
			this.collection.on('sync', this.render, this);
			this.collection.fetch();
			this.$el.on('add', this.addReciever, this);
			this.on('change', this.render, this);
		},
		defaults: {
			recieverClass		: '.js-add-new-receiver',
			contactBlockClass	: '.js-contact-block'
		},
		events: {
			'click .js-add-reciever' : 'addReciever'	
		},
		addReciever: function() {
			var reciever = this.collection.create({
				recieverAdress: $(this.defaults.recieverClass).val()
			});
			
			var recieverViewAdmin = new RecieverViewAdmin({model: reciever});
			this.$el.append(recieverViewAdmin.el);
			resetInput.call(this);
		},
		render: function() {
			this.$el.html(this.addTemplate(this.collection.toJSON()));
			this.collection.each(function(reciever) {
				var recieverViewAdmin = new RecieverViewAdmin({model: reciever});
				this.$el.append(recieverViewAdmin.el);
			}, this);
			$(this.defaults.contactBlockClass).append(this.$el);
		}
	});

	return RecieversCollectionViewAdmin;
});