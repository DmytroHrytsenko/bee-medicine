define(['jquery', 'underscore', 
		'backbone', 'ServCollection', 'ServViewCust', 
		'servTemplCustomer'], function($, _, Backbone, ServiceCollection, ServiceViewCustomer) {	
	var ServiceCollectionViewCustomer = Backbone.View.extend({
		tagName: 'div',
		defaults: {
			serviceClass: '.js-service',
			showOfferClass: 'show-offer'
		},
		initialize: function() {
			this.collection = new ServiceCollection();
			this.collection.on('sync', this.render, this)
			this.collection.fetch();
		},
		render: function () {
			var self = this;
			
			this.collection.each(function (service) {
				var serviceViewCustomer = new ServiceViewCustomer({ model: service });
				this.$el.append(serviceViewCustomer.el);
			}, this );
			
			$(window).scroll(function() {
				var w3scroll = $(this).scrollTop();
				$(self.defaults.serviceClass).each(function() {
					if (w3scroll > $(this).offset().top - ($(window).height()/12)) {
						$(this).addClass(self.defaults.showOfferClass);
					}
				});
			});
		}
	});
	
	return ServiceCollectionViewCustomer;
});


