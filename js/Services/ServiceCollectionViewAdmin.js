define(['jquery', 'underscore', 
		'backbone', 'ServCollection', 'servAddTempl', 
		'ServModel', 'ServViewAdm', 'backbone-mediator'], function($, _, Backbone, ServiceCollection, serviceAddTemplate, Service, ServiceViewAdmin) {	
	function resetInput () {
		this.$(this.defaults.nameClass).val('');
		this.$(this.defaults.priceClass).val('');
		this.$(this.defaults.descriptionClass).val('');
	};
	var ServiceCollectionViewAdmin = Backbone.View.extend({
		tagName: 'div',
		addTemplate: serviceAddTemplate,
		defaults: {
			nameClass		: '.js-add-new-service-name',
			priceClass		: '.js-add-new-service-price',
			descriptionClass: '.js-add-new-service-description',
		},
		events: {
			'click .js-add' 	: 'addService',
			'click .js-view-all': 'returnToCustomer'	
		},
		initialize: function() {
			this.$el.html(this.addTemplate);
			this.collection = new ServiceCollection();
			this.collection.on('sync', this.render, this);
			this.collection.fetch();
			this.on('change', this.render, this);
		},
		returnToCustomer: function() {
			Backbone.Mediator.publish('navigate', '/', {trigger: true});
		},
		addService: function() {
			var service = this.collection.create({
				serviceName: this.$(this.defaults.nameClass).val(),
				price: this.$(this.defaults.priceClass).val(),
				description: this.$(this.defaults.descriptionClass).val()
			});			
			var serviceViewAdmin = new ServiceViewAdmin({model: service});
			this.$el.append(serviceViewAdmin.el);
			resetInput.call(this);
		},
		render: function() {
			this.collection.each(function(service) {
				var serviceViewAdmin = new ServiceViewAdmin({model: service});
				this.$el.append(serviceViewAdmin.el);
			}, this);
		}
	});

	return ServiceCollectionViewAdmin;
});