define(['jquery', 'underscore', 
		'backbone', 'servTemplAdmin', 
		'servEditTempl'], function($, _, Backbone, serviceTemplateAdmin, serviceEditTemplate) {
	var ServiceViewAdmin = Backbone.View.extend({
		tagName: 'div',
		className: 'admin-block',
		displayTemplate	:  serviceTemplateAdmin,
		editTemplate    :  serviceEditTemplate,
		defaults: {
			nameClass		: '.js-edit-service',
			priceClass		: '.js-edit-price',
			descriptionClass: '.js-edit-description'
		},
		events: {
			'click .js-edit' 	: 'editService',
			'click .js-delete'	: 'deleteService',
			'click .js-update' 	: 'updateService',
			'click .js-cancel' 	: 'cancel'
		},
		initialize: function() {
			this.render();
		},
		render: function() {
			this.$el.html(this.displayTemplate(this.model.toJSON()));
			return this;
		},
		editService: function() {
			this.$el.html(this.editTemplate(this.model.toJSON()));
		},
		deleteService: function() {
			this.remove();
			this.model.destroy();
		},

		updateService:function() {
			this.model.set({ 	
							'serviceName': this.$(this.defaults.nameClass).val(),
							'price': this.$(this.defaults.priceClass).val(),
							'description': this.$(this.defaults.descriptionClass).val()
							});
			this.model.save();
			this.render();
		},
		cancel: function() {
			this.render();
		},
	});

	return ServiceViewAdmin;
});