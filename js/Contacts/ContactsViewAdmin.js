define(['jquery', 'underscore', 'backbone', 'contactsAdminTemplate', 'contactsAdminEditTemplate'], function($, _, Backbone, contactsAdminTemplate, contactEditTemplate) {	
	var ContactsViewAdmin = Backbone.View.extend({
		tagName: 'div',
		className: 'admin-block',
		adminTemplate: contactsAdminTemplate,
		editTemplate: contactEditTemplate,
		defaults: {
			addressClass: '.js-editAddress',
			phoneClass	: '.js-editTelephone',
			weekDayClass: '.js-editHoursWeekday',
			weekendClass: '.js-editHoursWeekend'
		},
		events: {
			'click .js-edit'   : 'editContact',
			'click .js-delete' : 'removeContact',
			'click .js-update' : 'updateContact',
			'click .js-cancel' : 'cancel'

		},
		initialize: function() {
			this.render();
		},
		render: function() {
			this.$el.html(this.adminTemplate(this.model.toJSON()));
			return this;
		},
		editContact: function() {
			this.$el.html(this.editTemplate(this.model.toJSON()));
		},
		removeContact: function () {
			this.remove();
			this.model.destroy();
		},
		updateContact: function() {
			this.model.set('address', this.$(this.defaults.addressClass).val());
			this.model.set('telephone', this.$(this.defaults.phoneClass).val());
			this.model.set('working_hours_weekday', this.$(this.defaults.weekDayClass).val());
			this.model.set('working_hours_weekend', this.$(this.defaults.weekendClass).val());
			this.model.save();
			this.render();
		},
		cancel: function() {
			this.render();
		}
	});

	return ContactsViewAdmin;
});