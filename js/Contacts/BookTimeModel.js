define(['backbone'], function(Backbone) {
	var BookTimeModel = Backbone.Model.extend({
		url: '/send-book-time'
	});

	return BookTimeModel;
});