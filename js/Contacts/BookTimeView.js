define(['jquery', 'underscore', 'backbone', 'bookTimeTemplate', 'BookTimeModel'], 
	function($, _, Backbone, bookTimeTemplate, BookTimeModel) {	
		function resetInputs() {
			this.$(this.defaults.nameClass).val('');
			this.$(this.defaults.phoneClass).val('');
			this.$(this.defaults.commentsClass).val('');
		};
		var BookTimeView = Backbone.View.extend({
			popUpDiv: bookTimeTemplate,
			defaults: {
				nameClass		: '.js-form-name',
				phoneClass		: '.js-form-phone',
				dateClass		: '.js-form-date',
				commentsClass	: '.js-form-comments',
				bookBlockClass	: '.js-book-block'
			},
			events: {
				'click' 				: 'render',
				'click .js-cancel' 		: 'closeForm', 
				'click .js-send' 		: 'sendOrder',
				'click .js-book-form' 	: 'prvntProp',
				'click .js-book-block'	: 'closeForm'
			},
			closeForm: function(e) {
				e.stopPropagation();
				this.$(this.defaults.bookBlockClass).remove();
			},
			render: function() {
				this.$el.append(this.popUpDiv);
			},
			sendOrder: function() {
				var form = new BookTimeModel({
					customerName	: $(this.defaults.nameClass).val(),
					customerPhone	: $(this.defaults.phoneClass).val(),
					dateOfProcedure	: $(this.defaults.dateClass).val(),
					customerComments: $(this.defaults.commentsClass).val()				
				});
				resetInputs.call(this);
				form.save();
			},
			prvntProp: function(e) {
				e.stopPropagation();
			}
		});

		return BookTimeView;
	});