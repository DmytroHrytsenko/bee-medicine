define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {

	var LoaderView = Backbone.View.extend({
		tagName: 'div',
		className: 'loader-block',
		loader: _.template('<div class="loader"></div>'),
		defaults: {
			mainClass: '.main'
		},
		initialize: function() {
			this.render();
		},
		render: function() {
			$(this.defaults.mainClass).append(this.$el.html(this.loader));
		}
	});

	return LoaderView;
});