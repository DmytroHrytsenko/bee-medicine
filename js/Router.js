define(['backbone', 'ServCollectionViewCust', 
	'ContactsCollectionViewCustomer', 'BookTimeView',
	'ServCollectionViewAdm', 'ContactsCollectionViewAdmin', 
	'RecieversCollectionViewAdmin', 'backbone-mediator'], function(Backbone, ServiceCollectionViewCustomer, ContactsCollectionViewCustomer, BookTimeView, ServiceCollectionViewAdmin, ContactsCollectionViewAdmin, RecieversCollectionViewAdmin) {
	var Router = Backbone.Router.extend({
		routes: {
			"admin" : "admin",
			"" 		: "home"
		},
		defaults: {
			serviceBlockClass	: '.js-service-block',
			contactBlockClass 	: '.js-contact-block',
			bookTimeClass		: '.js-book'
		},
		initialize: function() {
			Backbone.Mediator.subscribe('navigate', this.navigate, this);
		},
		home: function() {
			var serviceCollectionView = new ServiceCollectionViewCustomer();
			var contactsCollectionView = new ContactsCollectionViewCustomer();
			$(this.defaults.serviceBlockClass).html(serviceCollectionView.el);
			$(this.defaults.contactBlockClass).html(contactsCollectionView.el);

			$(this.defaults.bookTimeClass).each(function() {
				var bookTimeView = new BookTimeView({el: $(this)});
			});
		},
		admin: function() {
			
			var serviceCollectionView = new ServiceCollectionViewAdmin();
			var contactsCollectionView = new ContactsCollectionViewAdmin();
			var recieversCollectionViewAdmin = new RecieversCollectionViewAdmin();
			$(this.defaults.serviceBlockClass).html(serviceCollectionView.el);
			$(this.defaults.contactBlockClass).html(contactsCollectionView.el);
		}
	});

	return Router;
});